// multiresult.h
// Display result in multiple bases.
//
// This file is part of the SpeedCrunch project
// Copyright (C) 2020 @andreatta
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#ifndef MULTIRESULT_H
#define MULTIRESULT_H

#include "quantity.h"

#include <QWidget>
#include <QLabel>
#include <QPushButton>
#include <QGridLayout>

class QHBoxLayout;

class MultiResult : public QWidget {
    Q_OBJECT
    Q_DISABLE_COPY(MultiResult)

public:
    explicit MultiResult(QWidget* parent = 0);

    bool state() const { return m_state; }
    void setState(bool state);

public slots:
    void updateResult(const Quantity& result);

private:
    bool m_state;

    QGridLayout* layout;
    QLabel* labelDesc;
    QPushButton* labelResult;
    QList<QPushButton*> results;

private slots:
    void copyToClipboard();
};

#endif // MULTIRESULT_H
