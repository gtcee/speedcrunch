// multiresult.cpp
// Display result in multiple bases.
//
// This file is part of the SpeedCrunch project
// Copyright (C) 2020 @andreatta
//
// This program is free software; you can redistribute it and/or
// modify it under the terms of the GNU General Public License
// as published by the Free Software Foundation; either version 2
// of the License, or (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; see the file COPYING.  If not, write to
// the Free Software Foundation, Inc., 51 Franklin Street, Fifth Floor,
// Boston, MA 02110-1301, USA.

#include "multiresult.h"
#include <QClipboard>
#include <QApplication>
#include "gui/syntaxhighlighter.h"


QStringList titles = {"Integer", "Float", "Hexadecimal", "Octal", "Binary", "ASCII"};
enum eResultIndex {INT, FLOAT, HEX, OCT, BIN, ASCII};

MultiResult::MultiResult(QWidget* parent)
    : QWidget(parent)
{
    int row = 0;
    setObjectName("MultiResult");

    layout = new QGridLayout(this);

    foreach (auto t, titles) {
        labelDesc = new QLabel(this);
        labelDesc->setText(t);
        layout->addWidget(labelDesc, row, 0);

        labelResult = new QPushButton(this);
        labelResult->setFlat(true);
        labelResult->setStyleSheet("Text-align:left");
        labelResult->setToolTip("Click to copy to clipboard");
        connect(labelResult, SIGNAL(clicked()), this, SLOT(copyToClipboard()));
        results.append(labelResult);
        layout->addWidget(labelResult, row, 1);

        row++;
    }

    this->setMinimumSize(20,20);
}

void MultiResult::updateResult(const Quantity& result)
{
    Quantity numberInt = DMath::integer(result);

    results[INT]->setText(DMath::format(numberInt.numericValue(), Quantity::Format::Fixed()));
    results[FLOAT]->setText(DMath::format(result.numericValue(),  Quantity::Format::Fixed()));
    results[HEX]->setText(DMath::format(numberInt.numericValue(), Quantity::Format::Fixed() + Quantity::Format::Hexadecimal()));
    results[OCT]->setText(DMath::format(numberInt.numericValue(), Quantity::Format::Fixed() + Quantity::Format::Octal()));
    results[BIN]->setText(DMath::format(numberInt.numericValue(), Quantity::Format::Fixed() + Quantity::Format::Binary()));
    results[ASCII]->setText(QChar(result.numericValue().toInt()));
}

void MultiResult::copyToClipboard()
{
    QPushButton* buttonSender = qobject_cast<QPushButton*>(sender());
    QString text = buttonSender->text();

    QClipboard* clipboard = QApplication::clipboard();
    clipboard->setText(text, QClipboard::Clipboard);

    if (clipboard->supportsSelection()) {
        clipboard->setText(text, QClipboard::Selection);
    }
}

